#!/usr/bin/env Rscript
install.packages(c("DescTools","MASS","knitr","VennDiagram","pander"),repos="https://cloud.r-project.org")
require(knitr)
base.dir <- NULL
base.url <- "/"
fig.path <- "banco/images/"
opts_knit$set(base.dir = base.dir, base.url = base.url)
opts_chunk$set(fig.path = fig.path)
files=list.files(path='R',full.names=T,pattern = "[.]Rmd$")
for (f in files) knit(f)
files = list.files(pattern = "[.]md$")
for (f in files) file.rename(f,paste0('source/_posts/',f))
