title: "About the project"
date: 
description: "about"
type: "about"
---

The [project][proj] was submitted to the IRB of our institution and approved in 2014. [Hospital Infantil Albert Sabin (HIAS)][hias] is a pediatric health center that serves as tertiary referral for the advanced care of children and adolescents in the state of [Ceará, Brazil][ce]. The [Pediatric Cancer Center][cpc] is the hospital department that provides outpatient and inpatient care for children and adolescents with cancer. Recently, an expansion to the department was inaugurated that centralizes the care of pediatric patients with [central nervous system (CNS) tumors][cns]. 

<!--more-->

The neuro-oncology service of HIAS's Pediatric Cancer Center is the only referral for children and adolescents with CNS in the state of Ceará. Our state has an estimated population of more than 9 million in an area larger than 146,000 km<sup>2</sup>. The estimated age-adjusted incidence rate of CNS tumors reported by a population-based cancer registry was 13.4 per million [1]. Thus, the predicted 2019 number of new pediatric CNS tumor cases in our state is 38. 

The project collected data about 455 patients treated for various CNS tumours, both benign and malignant, in the period from 2000 to 2015. The information was thoroughly disindentified. The publication of this dataset in a third party repository ([Zenodo][zen]) followed the brazilian and international regulation for medical data. The complete unedited dataset can be accessed [here][data].

This project complies with [open science definition][osd] and the [OECD Principles and Guidelines for Access to Research Data from Public Funding][oecd]. This site uses strategies of [open notebook science][ons]. The posts are [Rmarkdown][rmd] files that make transparent the data manipulation and statistical calculations used to reach the results and conclusions depicted. To learn more and contribute with the project, go to its [repository][repo].

[1] de Camargo, B. , de Oliveira Santos, M. , Rebelo, M. S., de Souza Reis, R. , Ferman, S. , Noronha, C. P. and Pombo‐de‐Oliveira, M. S. (2010), Cancer incidence among children and adolescents in Brazil: First report of 14 population‐based cancer registries. Int. J. Cancer, 126: 715-720. doi:[10.1002/ijc.24799][doi]

[![](https://zenodo.org/badge/DOI/10.5281/zenodo.3576056.svg)](https://doi.org/10.5281/zenodo.3576056)

[hias]:http://www.hias.ce.gov.br/#
[ce]:https://en.wikipedia.org/wiki/Cear%C3%A1
[cpc]:https://goo.gl/maps/GifvAJNwHTRmDjtKA
[cns]:https://en.wikipedia.org/wiki/Central_nervous_system_tumor
[doi]:https://doi.org/10.1002/ijc.24799
[data]:https://gitlab.com/neuro-oncologia/banco/blob/master/data/snc2018.csv
[zen]:https://zenodo.org
[osd]:https://www.fosteropenscience.eu/foster-taxonomy/open-science-definition
[oecd]:https://www.oecd.org/science/sci-tech/38500813.pdf
[ons]:https://en.wikipedia.org/wiki/Open-notebook_science
[rmd]:https://rmarkdown.rstudio.com/lesson-1.html
[repo]:https://gitlab.com/neuro-oncologia/banco
[proj]:https://gitlab.com/neuro-oncologia/banco/-/blob/232143e8d24fc7966b859727f15f461a6af3107f/source/doc/proj/Anteprojeto_Banco_HIAS_2.pdf
