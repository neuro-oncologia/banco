title: "CNS tumor types"
date: 
type: "tiposd"
---

1. Tumours of neuroepithelial tissue
 1.1. Astrocytic tumours (Astrocytomas, a type of brain tumor)
  1.1.1. Pilocytic astrocytoma (ICD-O 9421/1, WHO grade I)
   1.1.1a. Pilomyxoid astrocytoma (ICD-O 9425/3, WHO grade II)
  1.1.2. Subependymal giant cell astrocytoma (ICD-O 9384/1, WHO grade I)
  1.1.3. Pleomorphic xanthoastrocytoma (ICD-O 9424/3, WHO grade II)
  1.1.4. Diffuse astrocytoma (ICD-O 9400/3, WHO grade II)
  1.1.5. Anaplastic astrocytoma (ICD-O 9401/3, WHO grade III)
  1.1.6. Glioblastoma (ICD-O 9440/3, WHO grade IV)
   1.1.6a. Giant cell glioblastoma (ICD-O 9441/3, WHO grade IV)
   1.1.6b. Gliosarcoma (ICD-O 9442/3, WHO grade IV)
  1.1.7. Gliomatosis cerebri (ICD-O 9381/3, WHO grade III)
 1.2. Oligodendroglial tumours
  1.2.1. Oligodendroglioma (ICD-O 9450/3, WHO grade II)
  1.2.2. Anaplastic oligodendroglioma (ICD-O 9451/3, WHO grade III)
 1.3. Oligoastrocytic tumours
  1.3.1. Oligoastrocytoma (ICD-O 9382/3, WHO grade II)
  1.3.2. Anaplastic oligoastrocytoma (ICD-O 9382/3, WHO grade III)
 1.4. Ependymal tumours
  1.4.1. Subependymoma (ICD-O 9383/1, WHO grade I)
  1.4.2. Myxopapillary ependymoma (ICD-O 9394/1, WHO grade I)
  1.4.3. Ependymoma (ICD-O 9391/3, WHO grade II)
  1.4.4. Anaplastic ependymoma (ICD-O 9392/3, WHO grade III)
 1.5. Choroid plexus tumours
  1.5.1. Choroid plexus papilloma (ICD-O 9390/0, WHO grade I)
  1.5.2. Atypical choroid plexus papilloma ( ICD-O 9390/1, WHO grade II)
  1.5.3. Choroid plexus carcinoma (ICD-O 9390/3, WHO grade III)
 1.6. Other neuroepithelial tumours
  1.6.1. Astroblastoma (ICD-O 9430/3, WHO grade I)
  1.6.2. Chordoid glioma of the third ventricle (ICD-O 9444/1, WHO grade II)
  1.6.3. Angiocentric glioma (ICD-O 9431/1, WHO grade I)
 1.7. Neuronal and mixed neuronal-glial tumours
  1.7.1. Dysplastic gangliocytoma of cerebellum (Lhermitte-Duclos) (ICD-O 9493/0)
  1.7.2. Desmoplastic infantile astrocytoma/ganglioglioma (ICD-O 9412/1, WHO grade I)
  1.7.3. Dysembryoplastic neuroepithelial tumour (ICD-O 9413/0, WHO grade I)
  1.7.4. Gangliocytoma (ICD-O 9492/0, WHO grade I)
  1.7.5. Ganglioglioma (ICD-O 9505/1, WHO grade I)
  1.7.6. Anaplastic ganglioglioma (ICD-O 9505/3, WHO grade III)
  1.7.7. Central neurocytoma (ICD-O 9506/1, WHO grade II)
  1.7.8. Extraventricular neurocytoma (ICD-O 9506/1, WHO grade II)
  1.7.9. Cerebellar liponeurocytoma (ICD-O 9506/1, WHO grade II)
  1.7.10. Papillary glioneuronal tumour (ICD-O 9509/1, WHO grade I)
  1.7.11. Rosette-forming glioneuronal tumour of the fourth ventricle (ICD-O 9509/1, WHO grade I)
  1.7.12. Paraganglioma (ICD-O 8680/1, WHO grade I)
 1.8. Tumours of the pineal region
  1.8.1. Pineocytoma (ICD-O 9361/1, WHO grade I)
  1.8.2. Pineal parenchymal tumour of intermediate differentiation (ICD-O 9362/3, WHO grade II, III)
  1.8.3. Pineoblastoma (ICD-O 9362/3, WHO grade IV)
  1.8.4. Papillary tumors of the pineal region (ICD-O 9395/3, WHO grade II, III)
 1.9. Embryonal tumours
  1.9.1. Medulloblastoma (ICD-O 9470/3, WHO grade IV)
   1.9.1b. Medulloblastoma with extensive nodularity (ICD-O 9471/3, WHO grade IV)
   1.9.1c. Anaplastic medulloblastoma (ICD-O 9474/3, WHO grade IV)
  1.9.2. CNS Primitive neuroectodermal tumour (ICD-O 9473/3, WHO grade IV)
   1.9.2a. CNS Neuroblastoma (ICD-O 9500/3, WHO grade IV)
  1.9.3. Atypical teratoid/rhabdoid tumour (ICD-O 9508/3, WHO grade IV)
2. Tumours of cranial and paraspinal nerves
 2.1. Schwannoma (ICD-O 9560/0, WHO grade I)
 2.2. Neurofibroma (ICD-O 9540/0, WHO grade I)
 2.3. Perineurioma (ICD-O 9571/0, 9571/3, WHO grade I, II, III)
 2.4. Malignant peripheral nerve sheath tumour (MPNST) (ICD-O 9540/3, WHO grade II, III, IV)
3. Tumours of the meninges
 3.1. Tumours of meningothelial cells
  3.1.1. Meningioma (ICD-O 9530/0, WHO grade I)
   3.1.1a. Atypical meningioma (ICD-O 9539/1, WHO grade II)
   3.1.1b. Anaplastic meningioma (ICD-O 9530/3, WHO grade III)
 3.2. Mesenchymal tumours
  3.2.1. Lipoma (ICD-O 8850/0)
  3.2.2. Angiolipoma (ICD-O 8861/0)
  3.2.3. Hibernoma (ICD-O 8880/0)
  3.2.4. Liposarcoma (ICD-O 8850/3)
  3.2.5. Solitary fibrous tumour (ICD-O 8815/0)
  3.2.6. Fibrosarcoma (ICD-O 8810/3)
  3.2.7. Malignant fibrous histiocytoma (ICD-O 8830/3)
  3.2.8. Leiomyoma (ICD-O 8890/0)
  3.2.9. Leiomyosarcoma (ICD-O 8890/3)
  3.2.10. Rhabdomyoma (ICD-O 8900/0)
  3.2.11. Rhabdomyosarcoma (ICD-O 8900/3)
  3.2.12. Chondroma (ICD-O 9220/0)
  3.2.13. Chondrosarcoma (ICD-O 9220/3)
  3.2.14. Osteoma (ICD-O 9180/0)
  3.2.15. Osteosarcoma (ICD-O 9180/3)
  3.2.16. Osteochondroma (ICD-O 9210/0)
  3.2.17. Haemangioma (ICD-O 9120/0)
  3.2.18. Epithelioid hemangioendothelioma (ICD-O 9133/1)
  3.2.19. Haemangiopericytoma (ICD-O 9150/1, WHO grade II)
  3.2.20. Anaplastic haemangiopericytoma (ICD-O 9150/3, WHO grade III)
  3.2.21. Angiosarcoma (ICD-O 9120/3)
  3.2.22. Kaposi Sarcoma (ICD-O 9140/3)
  3.2.23. Ewing Sarcoma - PNET (ICD-O 9364/3)
 3.3 Primary melanocytic lesions
  3.3.1. Diffuse melanocytosis (ICD-O 8728/0)
  3.3.2. Melanocytoma (ICD-O 8728/1)
  3.3.3. Malignant melanoma (ICD-O 8720/3)
  3.3.4. Meningeal melanomatosis (ICD-O 8728/3)
 3.4. Other neoplasms related to the meninges
  3.4.1. Haemangioblastoma (ICD-O 9161/1, WHO grade I)
4. Tumors of the haematopoietic system
 4.1. Malignant Lymphomas (ICD-O 9590/3)
 4.2. Plasmocytoma (ICD-O 9731/3)
 4.3. Granulocytic sarcoma (ICD-O 9930/3)
5. Germ cell tumours
 5.1. Germinoma (ICD-O 9064/3)
 5.2. Embryonal carcinoma (ICD-O 9070/3)
 5.3. Yolk sac tumour (ICD-O 9071/3)
 5.4. Choriocarcinoma (ICD-O 9100/3)
 5.5. Teratoma (ICD-O 9080/1)
 5.6. Mixed germ cell tumours (ICD-O 9085/3)
6. Tumours of the sellar region
 6.1. Craniopharyngioma (ICD-O 9350/1, WHO grade I)
 6.2. Granular cell tumour (ICD-O 9582/0, WHO grade I)
 6.3. Pituicytoma (ICD-O 9432/1, WHO grade I)
 6.4. Spindle cell oncocytoma of the adenohypophysis (ICD-O 8991/0, WHO grade I)
7. Metastatic Tumours
Tumors that originate outside CNS and spread secondarily to CNS via Hematogenous route or by direct invasion from adjacent tissues. Most common primary tumour site
 7.1. Lung
 7.2. Breast
 7.3. Melanoma
 7.4. Renal
 7.5. Colorectal

Reference : WHO pathology of tumours of central nervous system-2007
