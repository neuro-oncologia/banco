title: "publications"
date: 
description: "docs"
type: "docs"
---

Whitepapers and partial results of the [project][proj] were drafted and published. Some documents were for internal circulation at the [Pediatric Cancer Center][cpc] of the [Hospital Infantil Albert Sabin (HIAS)][hias]. The publications listed here were the first ever scientific published articles about pediatric [central nervous system (CNS) tumors][cns] published at our institution and also in the state of [Ceará, Brazil][ce]. 

<!--more-->

Publications in peer-reviewed journals:

[1] Felix FH. Primary diffuse leptomeningeal gliomatosis: a rare disease in pediatric patients. J Pediatr (Rio J). 2009 May-Jun;85(3):277-9; author reply 279-80. English, Portuguese. doi: [10.2223/JPED.1900][jped1]. PMID: 19492175.
[2] Araujo OL, Trindade KM, Trompieri NM, Fontenele JB, Felix FH. Analysis of survival and prognostic factors of pediatric patients with brain tumor. J Pediatr (Rio J). 2011 Sep-Oct;87(5):425-32. English, Portuguese. doi: [10.2223/JPED.2124][jped2]. Epub 2011 Sep 30. PMID: 21964994.
[3] Felix FH, Trompieri NM, de Araujo OL, da Trindade KM, Fontenele JB. Potential role for valproate in the treatment of high--risk brain tumors of childhood-results from a retrospective observational cohort study. Pediatr Hematol Oncol. 2011 Oct;28(7):556-70. doi: [10.3109/08880018.2011.563774][pho]. Epub 2011 Jun 24. PMID: 21699466.
[4] Felix FH, de Araujo OL, da Trindade KM, Trompieri NM, Fontenele JB. Survival of children with malignant brain tumors receiving valproate: a retrospective study. Childs Nerv Syst. 2013 Feb;29(2):195-7. doi: [10.1007/s00381-012-1997-0][cns]. Epub 2012 Dec 12. PMID: 23233213.
[5] Felix FH, de Araujo OL, da Trindade KM, Trompieri NM, Fontenele JB. Retrospective evaluation of the outcomes of children with diffuse intrinsic pontine glioma treated with radiochemotherapy and valproic acid in a single center. J Neurooncol. 2014 Jan;116(2):261-6. doi: [10.1007/s11060-013-1280-6][jno]. Epub 2013 Nov 30. PMID: 24293221.
[6] Felix F, Fontenele J. Valproic Acid May Be Tested in Patients With H3F3A-Mutated High-Grade Gliomas. J Clin Oncol. 2016 Sep 1;34(25):3104-5. doi: [10.1200/JCO.2016.67.1073][jco]. Epub 2016 Jun 13. PMID: 27298407.

Preprints:

[7] Felix, F.H.C.; Fontenele, J.B. Predictors of Survival in Children with Ependymoma from a Single Center: Using Random Survival Forests. Preprints 2016, 2016110028 (doi: [10.20944/preprints201611.0028.v1][pp]).

Whitepapers:

[8] Felix, F.H.C. [Report of project results for internal circulation, 2014][rep]. Portuguese. 
[9] Felix, Francisco. (2016). Longitudinal observational study of pediatric patients with primary brain tumors: establishment of a hospital-based registry. (1.0.0) [Data set]. Zenodo. [![](https://zenodo.org/badge/DOI/10.5281/zenodo.3576056.svg)](https://doi.org/10.5281/zenodo.3576056)

[hias]:http://www.hias.ce.gov.br/#
[ce]:https://en.wikipedia.org/wiki/Cear%C3%A1
[cpc]:https://goo.gl/maps/GifvAJNwHTRmDjtKA
[cns]:https://en.wikipedia.org/wiki/Central_nervous_system_tumor
[doi]:https://doi.org/10.1002/ijc.24799
[data]:https://gitlab.com/neuro-oncologia/banco/blob/master/data/snc2018.csv
[zen]:https://zenodo.org
[osd]:https://www.fosteropenscience.eu/foster-taxonomy/open-science-definition
[oecd]:https://www.oecd.org/science/sci-tech/38500813.pdf
[ons]:https://en.wikipedia.org/wiki/Open-notebook_science
[rmd]:https://rmarkdown.rstudio.com/lesson-1.html
[repo]:https://gitlab.com/neuro-oncologia/banco
[proj]:https://gitlab.com/neuro-oncologia/banco/-/blob/232143e8d24fc7966b859727f15f461a6af3107f/source/doc/proj/Anteprojeto_Banco_HIAS_2.pdf
[jped1]:https://gitlab.com/neuro-oncologia/jped/-/blob/3719eb4747dc55fd29e5d2ebb29e8de3c16d93d4/1/5335_carta_eng.pdf
[jped2]:https://gitlab.com/neuro-oncologia/jped/-/blob/master/2/trab/275en.pdf
[pho]:https://doi.org/10.3109/08880018.2011.563774
[cns]:https://doi.org/10.1007/s00381-012-1997-0
[jno]:https://doi.org/10.1007/s11060-013-1280-6
[jco]:https://doi.org/10.1200/jco.2016.67.1073
[pp]:https://www.preprints.org/manuscript/201611.0028/v1
[rep]:https://gitlab.com/neuro-oncologia/banco/-/blob/232143e8d24fc7966b859727f15f461a6af3107f/source/doc/report/Novo_banco_2014.pdf
