[![Pipeline badge](https://gitlab.com/neuro-oncologia/banco/badges/master/pipeline.svg)](https://gitlab.com/neuro-oncologia/banco/commits/master)
[![](https://zenodo.org/badge/DOI/10.5281/zenodo.3576056.svg)](https://doi.org/10.5281/zenodo.3576056)
## Banco de dados de pacientes pediátricos com tumores cerebrais

### O que é:

Repositório do projeto de pesquisa do banco de dados do Registro Hospitalar de Tumores do Sistema Nervoso Central (SNC) do Hospital Infantil Albert Sabin.

## Objetivo:

Armazenar os dados sobre crianças e adolescentes diagnosticados com tumores do SNC e tratados no Hospital Infantil Albert sabin entre 2000 e 2015.

## Visão:

Estimular a pesquisa e melhorar a qualidade de vida de crianças e adolescentes com tumores do SNC.

## Missão:

Dar suporte a projetos de pesquisa envolvendo o tratamento de crianças e adolescentes com tumores do SNC.

## Declaração ética: 

Os dados publicados nesta base são desidentificados e obedecem a regulamentação ética brasileira e internacional sobre dados de saúde. 

## Como contribuir:

- Crie uma conta no [Gitlab][lab] (gratuita) e abra uma **questão** ([_issue_][issue]).
- **Duplique** ([_fork_][fork]) o repositório, faça alterações e **peça uma emenda** ([_merge request_][merge]).

## Como citar:

Felix, Francisco. (2016). Longitudinal observational study of pediatric patients with primary brain tumors: establishment of a hospital-based registry. (Version 1.0.0) [Data set]. Zenodo. http://doi.org/10.5281/zenodo.3576056

-------

## Database of pediatric brain tumor patients

### Definition:

Repository of the Central Nervous System Tumor Registry (CNS) database research project of the Albert Sabin Children's Hospital.

## Goal:

Store data on children and adolescents diagnosed with CNS tumors and treated at Hospital Infantil Albert Sabin between 2000 and 2015.

## Vision:

Stimulate research and improve the quality of life of children and adolescents with CNS tumors.

## Mission:

Support research projects involving the treatment of children and adolescents with CNS tumors.

## Ethical disclaimer:

The data published in this dataset are fully desidentified and this project follows brazilian and international ethical regulation on medical data.

## How to contribute:

- Create an account in [Gitlab][lab] (free) and open an [_issue_][issue].
- [_Fork_][fork] the repository, make changes and make a [_merge request_][merge].

## How to cite:

Felix, Francisco. (2016). Longitudinal observational study of pediatric patients with primary brain tumors: establishment of a hospital-based registry. (Version 1.0.0) [Data set]. Zenodo. http://doi.org/10.5281/zenodo.3576056

[lab]: https://gitlab.com
[issue]: https://gitlab.com/neuro-oncologia/banco/issues
[fork]: https://gitlab.com/neuro-oncologia/banco/forks/new
[merge]: https://gitlab.com/dashboard/merge_requests?assignee_id=592949